<H2>Наши работы:</H2>
<div>
    <?php foreach ($works AS $work): ?>
        <ul>
            <li><?=$work['title']?></li>
            <li><?=$work['text']?></li>
            <li><a href="/articles/work/<?=$work['id']?>">Подробнее</a></li>
        </ul>
        <br>
    <?php endforeach; ?>
</div>