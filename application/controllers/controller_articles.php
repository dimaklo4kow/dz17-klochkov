<?php

/**
 * Created by PhpStorm.
 * User: 8
 * Date: 24.01.2017
 * Time: 10:48
 */
class controller_articles extends Controller
{
    function __construct()
    {
        $this->model = new Model_Articles;
        parent::__construct();
    }

    function action_index()
    {
        $data['works'] = $this->model->getAllWorks();
        $this->view->generate('articles_all_view.php', 'template_view.php', $data);
    }

    public function action_work($id){
        $data['work'] = $this->model->getWorkById($id);
        $this->view->generate('articles_work_view.php', 'template_view.php', $data);
    }
}